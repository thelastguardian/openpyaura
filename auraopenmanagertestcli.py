#!/usr/bin/env python3
import threading
import traceback
import time

from daemon.auradaemonclient import AuraDaemonClient
from daemon.auracommandsink import AuraCommandSink
from controllers.auracontrollerbase import AuraController as acbac
import daemon.daemoncommandmanagers as dcm
from utils import colors


class AuraOpenManagerBasic(AuraCommandSink):
    host = '10.0.0.41'
    port = 8484

    def __init__(self):
        print('Initing AuraOpenManager')
        self.init_daemon_client()
        # self.connect_daemon_client()

        #
        self.count = 0

    def init_daemon_client(self):
        self.daemon_client = AuraDaemonClient(self, self.host, self.port)

    def run(self):
        self.daemon_client.connect()
        controllers = self.daemon_client.get_remote_controller_details()
        for controller in controllers:
            print('---------------------')
            print(controller)
            # self.set_rgb_data(acbac.AURA_CONTROL_MODE_EFFECT, acbac.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [utils.red])
            # time.sleep(2)
            self.set_rgb_data(acbac.AURA_CONTROL_MODE_EFFECT, acbac.AURA_EFFECT_MODES['AURA_MODE_STATIC'], [colors.blue])
            # time.sleep(2)
            break

        
    def check_response(self, response):
        # print(response)
        if response['status'] == 'ok':
            return response['data']
        else:
            # print(response)
            raise Exception(response['error'])

    def send_to_remote(self, datadict):
        # datadict['command_manager'] = dcm.TYPE_BASIC_MODE
        # print('datadict', datadict)
        return self.daemon_client.send_to_remote(datadict, command_manager=dcm.TYPE_BASIC_MODE)

    def dump_aura_device(self):
        raise NotImplementedError('dump_aura_device not implemented in ', self)

    def get_rgb_data(self, mode):
        raise NotImplementedError('get_rgb_data not implemented in ', self)

    def set_rgb_data(self, control_mode, effect_mode, rgb_list):
        """
        rgb_list is list of (R,B,G) tuples of hex/int
        """
        data = {'command': 'set_rgb_data', 'args': [
            control_mode, effect_mode, rgb_list]}
        return self.send_to_remote(data)

    def get_effect_mode(self):
        data = {'command': 'get_effect_mode'}
        return self.send_to_remote(data)

    def get_control_mode(self):
        data = {'command': 'get_control_mode'}
        return self.send_to_remote(data)

    def set_control_mode(self, mode):
        data = {'command': 'set_control_mode', 'args': [mode]}
        return self.send_to_remote(data)

    def set_effect_mode(self, mode):
        data = {'command': 'set_effect_mode', 'args': [mode]}
        return self.send_to_remote(data)

    def apply_changes(self):
        data = {'command': 'apply_changes'}
        return self.send_to_remote(data)


def main():
    aura_open_manager = AuraOpenManagerBasic()
    aura_open_manager.run()

if __name__ == "__main__":
    main()