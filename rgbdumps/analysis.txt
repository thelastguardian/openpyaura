# analysis.txt
$ all colors are in R B G style NOT R G B
dump3 - all white, all areas, static color
dump4 - all blue, by area, backio white, static
dump5 - all white, by area, pch = 255 255 0, static
dump6 - all white, by area, rgb-strip-1 = 255 255 0, static
dump7 - all white, by area, rgb-strip-2 = 255 255 0, static
RGB ranges:

_strt_   __1__ __2__ __3__ __4__
0x8160 - R B G R B G R B G R B G
0x821c - R B G R B G R B G R B G
0x839f - R B G R B G R B G R B G
0x84fb - R B G R B G R B G R B G

_strt_   __1__ __2__ __3__ __4__
0x81C0 - R B G R B G R B G R B G - sync on shutdown set to static
0x83FF - R B G R B G R B G R B G - sync on shutdown set to static
(both ranges were 0 0 0 * 4 when sync on shutdown was set to wave mode,
so this could be the internal-effect-mode rgb range for sync on shutdown)
0x849e became 0xAE
0x8324 became 0x01

_strt_ = start address of rgb range (ranges arent all aligned to 0x*0 boundaries unlike RAM)
1 = backio
2 = pch
3 = rgb-strip-1
4 = rgb-strip-2

changed besides RGB ranges:

between allwhite all-areas mode to by-area mode:
0x8558 changed from 20 to 30 (seen in all subsequent dumps based on this by-area check)

between dump3 and dump4:
0x8558 changed from 20 to 30
0x849e changed from 66 to 6C

between dump3 and dump5:
0x8558 changed from 20 to 30
0x849e changed from 66 to 67

between dump3 and dump6:
0x8558 changed from 20 to 30
0x849e changed from 66 to 67
all rgb ranges changed as expected BESIDES range 0x821c, where middle value changed to 0 (B) instead of last value (G)

between dump3 and dump7:
0x8558 changed from 20 to 30
0x849e changed from 66 to 67
all rgb ranges changed as expected BESIDES range 0x821c, where middle value changed to 0 (B) instead of last value (G)
0x8555 changed from 01 to 00 (maybe some extra change i didnt notice or do before?) (when i went back to by-area all-white static, back to 01)

between dump3 and dump6-but-0-0-0 instead of ff-ff-ff:
0x8558 changed from 20 to 30
0x849e changed from 66 to 72


changing the calibration of rgb header-1 doesn't change anything in the dump of 0x4e
(besides 0x8558 changed from 20 to 30, which i guess is due to calibration mode going to by-area mode from all-areas mode)



# control regs:

0x8025 = "AURA_REG_MODE"
This register selects the Aura controller effect.
01 = static
04 = color cycle
05 = rainbow

0x8324 = same as above?


0x849e = changed from 5A to 54 when going from s-o-s wave to s-o-s rainbow, why?
0x849e = changed from 5A to 53 when going from s-o-s wave to s-o-s color cycle, why?