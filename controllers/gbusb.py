# pyusb.py
import hid
import time
import random
import os
import queue
from collections import abc as collabc

from openpyaura.controllers import rgbcontrollerbase
from openpyaura.utils import colors

# using hidapi for this
# could continue using pyusb, in which case: 
# dev.ctrl_transfer(0x21, 0x09, 0x300, 0x00, [0x1c,command,r,g,b,0x08,0x01,0x00], 10)
# dunno why 0x300 instead of 0x03 from the capture spec



RGB_EFFECT_MODE_UNKNOWN = -1

SUPPORTED_EFFECT_MODES = {
    'OFF': 0,
    'STATIC': 1,
    'PULSE': 2,
    'COLOR CYCLE': 3,
    'FLASH': 4, # ?? greyed out in selLed.exe
    'DOUBLE FLASH': 5, # ?? greyed out in selLed.exe
    'DEFAULT': 1, # ?? greyed out in selLed.exe
}

# modes that "don't need a color, like color cycle"

EFFECT_SPEEDS = {
    '1/6': 0x0a,
    '2/6': 0x0a,
    '3/6': 0x09,
    '4/6': 0x08,
    '5/6': 0x07,
    '6/6': 0x06,
}

def flatten(l):
    for el in l:
        if isinstance(el, collabc.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

class GigabyteSimpleUSBController(rgbcontrollerbase.RGBControllerBase):
    led_count = 1

    command_messages_to_apply = None

    device_type = 'RGBFUSION-USB-SIMPLE'

    def __init__(self, usb_params, friendly_name):
        self.device_id = ''.join([random.choice('ABCD1234') for i in range(5)])
        
        # this device uses URB Control Transfers, not interrupts.
        # much simpler to use since there's only one Control Endpoint for the device
        self.dev = hid.device()
        try:
            self.dev.open(usb_params[0], usb_params[1])
        except OSError as err:
            raise ValueError('Device not found. Error: ', str(err))

        self.friendly_name = friendly_name
        self.controller_name = self.friendly_name + ' - ' + self.dev.get_product_string() + ' - ' + self.dev.get_manufacturer_string()
        print('Inited', self.controller_name)
        self.verbose_name = self.controller_name + ' - GigabyteSimpleUSBController at ' + str(usb_params)
        self.command_messages_to_apply = queue.Queue()

    def __repr__(self):
        return self.verbose_name

    def __str__(self):
        return self.friendly_name

    def __json__(self):
        return {'id': self.device_id, 'controller_name': self.controller_name, 'type': self.device_type, 'friendly_name': self.friendly_name, 'device': str(self.dev)}

    def get_led_count(self):
        return self.led_count

    def get_supported_effect_modes(self):
        return SUPPORTED_EFFECT_MODES

    def get_control_mode(self):
        # self.in_ep.read
        return RGB_EFFECT_MODE_UNKNOWN
        # raise NotImplementedError(
        #     'get_control_mode in USB controller not implemented.')

    def get_effect_mode(self):
        # self.in_ep.read
        return RGB_EFFECT_MODE_UNKNOWN
        # raise NotImplementedError(
        #     'get_effect_mode in USB controller not implemented.')

    def set_effect_mode(self, mode):
        pass
        # NOTE: this is ignored here, because we send the effect mode in all RGB data commands
        # there is currently no known way/msg to set just the effect mode without an rgb set
        # self.set_rgb_data(auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT, mode, [colors.black])
        # self._create_command_msgs(self.AURA_CONTROL_MODE_EFFECT, self.device, mode, [colors.black])

    def set_control_mode(self, mode):
        # no such thing for this device
        pass

    # hex_msg is str, does not have leading 0x
    def write_hex_string_msg(self, hex_msg):
        if len(hex_msg) != 16:
            raise Exception('hex msg len not 16: ' + str(len(hex_msg)))
        # print('sending msg', hex_msg)
        bytes_written = self.dev.send_feature_report(bytes.fromhex('21' + hex_msg))
        # if bytes_written != 32:
        print('bytes written', bytes_written)
        return bytes_written

    # rgb_list is a list of self.led_count (R,B,G) tuples
    def _set_rgb_data(self, control_mode, effect_mode, rgb_list):
        print('_set_rgb_data', self, control_mode, effect_mode, rgb_list)
        cmd_msgs = self._create_command_msgs(effect_mode, rgb_list)
        for msg in cmd_msgs:
            self.command_messages_to_apply.put(msg)
        self.apply_changes()

    def set_rgb_data(self, control_mode, effect_mode, rgb_list):
        # control mode is unused, need to refactor all this to allow non-aura devices better
        print('set_rgb_data', self, control_mode, effect_mode, rgb_list)
        self._set_rgb_data(None, effect_mode, rgb_list)

    def get_rgb_data_by_mode(self, control_mode):
        # TODO: handle this "correctly?"
        return [colors.black]
        # raise NotImplementedError(
        #     'Reading from USB controller not implemented.')

    def get_rgb_data(self):
        # raise NotImplementedError(
        #     'Reading from USB controller not implemented.')
        return self.get_rgb_data_by_mode(self.get_control_mode())

    # pass rgb_tuple as (0xff,0xff,0xff) (strings) for now

    def _create_msg(self, command_type, data_vals):
        # data_vals is rgb_tuples for command_type = c8 (set colors?)
        # data vals is something?? for command_type = c9 (set effect mode)
        # data vals is somethingelse?? (all zeroes?) for command_type = b6 (apply??)
        print('_create_msg', command_type, data_vals)
        # assert len(rgb_tuples) == 20
        # one msg is 65 bytes. 5 bytes of mode info, 60 bytes of rgb data
        msg = format(0x01, '02x')
        msg += format(command_type, '02x')
        # print(msg)
        # assert len(msg) == 10
        # 60 bytes of rgb
        count = 0
        # flatten the iterable of iterables if needed
        data_vals = list(flatten(data_vals))
        for val in data_vals:
            msg += format(val, '02x')
            count += 1
            # if count == 3 and effect_mode in self.MODES_TO_LIMIT_TO_JUST_ONE_RGB_POINT:
                # break
        # include these in the caller-passed data_vals since these vary
        # msg += format(0x08, '02x')
        # msg += format(0x01, '02x')
        # msg += format(0x00, '02x')
        print(msg)
        # assert count <= 60
        # msg += ''.join('00'*(60-count))
        # if len(msg) != 130:
        #     raise Exception('msg len is ' + str(len(msg)))
        return msg

    def _create_command_msgs(self, effect_mode: int, rgb_tuples: list, effect_speed=EFFECT_SPEEDS['4/6']):
        assert len(rgb_tuples) >= 1
        # while(len(rgb_tuples) < 120):
        #     rgb_tuples += rgb_tuples
        # rgb_tuples = rgb_tuples[:120]
        # try:
        #     assert len(rgb_tuples) == 120
        # except AssertionError:
        #     raise Exception(rgb_tuples)
        
        print('-------------------------------------------------------------------------------')
        print('_create_command_msgs', self, effect_mode, rgb_tuples)
        # rgb_tuples = rgb_tuples[:20]
        set_colors_msg = self._create_msg(0xc8, rgb_tuples + [0x08, 0x01, 0x00])
        set_effect_mode = self._create_msg(0xc9, [effect_mode] + [0x09, effect_speed, 0x01, 0x08, 0x00])
        # when effect_mode = 0x04 or 0x05 (flash/doubleflash), next byte is 5a, but otherwise, is 09
        # assuming that is flash interval time/hold time. need to work that into controls.
        apply_msg = self._create_msg(0xb6, [0x00] + [0x00, 0x00, 0x00, 0x00, 0x00])
        return [set_colors_msg, set_effect_mode, apply_msg]

    def apply_changes(self):
        while True:
            try:
                msg = self.command_messages_to_apply.get(timeout=0.1)
                self.write_hex_string_msg(msg)
            except Exception as err:
                print(err)
                break


def get_rand_tuple():
    return (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

def test_method_effect(controllers):
    for controller in controllers:
        controller.set_rgb_data(None, 0, [colors.red])
        time.sleep(2)
        controller.set_rgb_data(None, 0, [colors.green])
        time.sleep(2)
        controller.set_rgb_data(None, 0, [colors.blue])
    # time.sleep(2)
    # for controller in controllers:
    #     controller.set_rgb_data(None, 0, [colors.red])
    #     time.sleep(2)


GigabyteRGBFusionSimpleUSBDevices = {
    (0x1044, 0x7a30): 'Gigabyte AC300W Case',
}

def detect_and_init_devices():
    # detections list
    controllers = []
    added_already = []
    for d in hid.enumerate():
        usb_params = (d['vendor_id'], d['product_id'])
        # print(usb_params)
        # print(d)
        if usb_params in GigabyteRGBFusionSimpleUSBDevices and usb_params not in added_already:
            controllers.append(GigabyteSimpleUSBController(usb_params, GigabyteRGBFusionSimpleUSBDevices[usb_params]))
            added_already.append(usb_params)
    print("Gigabyte RGBFusion Simple USB Devices detected: ", controllers)
    return controllers


def main():
    print('test')
    controllers = detect_and_init_devices()

    test_method_effect(controllers)
        # test_method_arb(controller)
        # test_method_effect(controller)
        # test_method_direct(controller)
    print('done')

if __name__ == "__main__":
    exit(main())
