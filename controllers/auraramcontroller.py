import sys
import random

from openpyaura.controllers import auracontrollerbase


def flip_endian_16(input_val: int):
    return (input_val << 8) & 0xff00 | (input_val >> 8) & 0x00ff


def hexstrip(int_val):
    return hex(int_val)[2:] if int_val is not None else 'XX'


class AuraI2CConfigConstants:

    # register addresses
    
    AURA_BYTE_READ_ADDR = 0x81
    AURA_BYTE_WRITE_ADDR = 0x01

    # register range storing plaintext device name
    AURA_REG_DEVICE_NAME = 0x1000

    # the register storing the control mode (0x00 = direct, 0x01 = effect)
    AURA_REG_DIRECT = 0x8020

    # the register storing the effect mode(0-14, see auracontrollerbase.py)
    AURA_REG_MODE = 0x8021

    # the register to which you have to write AURA_REG_APPLY_VAL, to "apply changes"
    AURA_REG_APPLY = 0x80A0
    AURA_REG_APPLY_VAL = 0x01

    AURA_REG_COLORS_DIRECT = {
        'LED-0116': 0x8000,
        'DIMM_LED-0102': 0x8000,
        'AUDA0-E6K5-0101': 0x8100,
        'AUMA0-E6K5-0105': 0x8100,
        'AUMA0-E6K5-0106': 0x8100,
    }
    AURA_REG_COLORS_DIRECT_DEFAULT = 0x8000
    AURA_REG_COLORS_EFFECT = {
        'LED-0116': 0x8010,
        'DIMM_LED-0102': 0x8010,
        'AUDA0-E6K5-0101': 0x8160,
        'AUMA0-E6K5-0105': 0x8160,
        'AUMA0-E6K5-0106': 0x8160,
    }
    AURA_REG_COLORS_EFFECT_DEFAULT = 0x8010

    
    # WIP, not confirmed:
    LED_COUNT = {
        'LED-0116': 5,
        'DIMM_LED-0102': 5,
        'AUDA0-E6K5-0101': 5,
        'AUMA0-E6K5-0105': 4,
        'AUMA0-E6K5-0106': 5,
    }
    LED_COUNT_DEFAULT = 5
    
    # WIP, not confirmed:
    AURA_LED_LABEL_LOOKUP_START = 0x1c13
    LED_LABEL = {
        0x05: 'UNKNOWN', # DRAM?
        0x84: 'UNKNOWN', # IO?
        0x86: 'UNKNOWN', # RGB Header 1?
        0x87: 'UNKNOWN', # RGB Header 2?
        0x8A: 'UNKNOWN',
    }
    LED_LABEL_DEFAULT = 'UNKNOWN'

    
    # WIP, not confirmed:
    AURA_LED_LOCATION_LOOKUP_START = 0x1c1b

class AuraRAMController(auracontrollerbase.AuraController):

    AURA_ADDR_START_RANGE = 0x8000
    AURA_ADDR_END_RANGE = 0x80ff

    device_type = 'AURAI2C'

    def __init__(self, bus_number, i2c_addr, friendly_name='Aura I2C Controller Generic', skip_aura_check=True):
        self.device_id = ''.join([random.choice('ABCD1234') for i in range(5)])
        self.bus_number = bus_number
        self.i2c_addr = i2c_addr
        from smbus2 import SMBus
        self.smbus = SMBus(bus=bus_number)
        self.friendly_name = friendly_name
        self.internal_name = ''
        if self.is_valid_aura_device() or skip_aura_check:
            self.internal_name = self.get_name_from_firmware()
            self.load_aura_config()

        self.controller_name = self.internal_name + ' - ' + self.friendly_name + \
            ' - AuraRAMController at SMBUS {} {:#x}'.format(
                self.bus_number, self.i2c_addr)

    def load_aura_config(self):
        self.AURA_REG_COLORS_DIRECT = AuraI2CConfigConstants.AURA_REG_COLORS_DIRECT.get(self.internal_name, AuraI2CConfigConstants.AURA_REG_COLORS_DIRECT_DEFAULT)
        self.AURA_REG_COLORS_EFFECT = AuraI2CConfigConstants.AURA_REG_COLORS_EFFECT.get(self.internal_name, AuraI2CConfigConstants.AURA_REG_COLORS_EFFECT_DEFAULT)
        self.AURA_REG_DIRECT = AuraI2CConfigConstants.AURA_REG_DIRECT
        self.AURA_REG_MODE = AuraI2CConfigConstants.AURA_REG_MODE
        self.AURA_REG_APPLY = AuraI2CConfigConstants.AURA_REG_APPLY
        self.AURA_REG_APPLY_VAL = AuraI2CConfigConstants.AURA_REG_APPLY_VAL
        self.led_count = AuraI2CConfigConstants.LED_COUNT.get(self.internal_name, AuraI2CConfigConstants.LED_COUNT_DEFAULT)
        self.led_list = []
        self.led_labels = []
        for i in range(self.led_count):
            led = self.AuraRegisterRead(AuraI2CConfigConstants.AURA_LED_LABEL_LOOKUP_START + i)
            self.led_list.append(led)
            self.led_labels.append(AuraI2CConfigConstants.LED_LABEL.get(led, AuraI2CConfigConstants.LED_LABEL_DEFAULT))

    def get_led_count(self):
        return self.led_count

    def __repr__(self):
        return self.controller_name

    def __str__(self):
        return self.friendly_name
        # return self.friendly_name

    def __json__(self):
        basic_info = {'id': self.device_id, 'controller_name': self.controller_name, 'type': self.device_type, 'friendly_name': self.friendly_name, 'led_count': self.led_count}
        device_info = {'bus_number': self.bus_number, 'i2c_addr': self.i2c_addr, 'smbus': str(self.smbus)}
        additional_debug_info = {'led_list': self.led_list, 'led_labels': self.led_labels,
            'self.AURA_REG_COLORS_DIRECT': self.AURA_REG_COLORS_DIRECT,
            'self.AURA_REG_COLORS_EFFECT': self.AURA_REG_COLORS_EFFECT,
            'self.internal_name' : self.internal_name,
        }
        json_ret = {}
        json_ret.update(basic_info)
        json_ret.update(device_info)
        json_ret.update(additional_debug_info)
        return json_ret

    def is_valid_aura_device(self):
        # a hacky is_this_an_aura_device() check
        # for now, try to read 0x00 on the bus and see if it succeeds
        # if so, could be aura controller, check memory space 0xA0 to 0xAF for 0-F bytes
        # if XX, no such device, so say no
        try:
            # print('Reading byte from address', iospace_reg)
            bdata = self.smbus.read_byte_data(
                self.i2c_addr, 0x00)
            for i in range(0xA0, 0xB0):
                if self.smbus.read_byte_data(self.i2c_addr, i) != (i - 0xA0):
                    return False
            return True
        except OSError:
            # Error 6: No such device or address => XX
            return False
        return False

    def get_name_from_firmware(self):
        name_chars = []
        for i in range(AuraI2CConfigConstants.AURA_REG_DEVICE_NAME, AuraI2CConfigConstants.AURA_REG_DEVICE_NAME + 0x10):
            val = self.AuraRegisterRead(i)
            if val:
                name_chars.append(chr(val))
            else:
                break
        return ''.join(name_chars).strip()

    def AuraRegisterRead(self, register: int):
        # print('Register: '  + str(register))
        # print('Reading reg: ' + hex(flip_endian_16(register)))
        try:
            # print('Writing address', hex(register), 'to 0x00')
            self.smbus.write_word_data(self.i2c_addr, 0x00,
                                       flip_endian_16(register))
            # print('Reading byte from address', AURA_BYTE_READ_ADDR)
            bdata = self.smbus.read_byte_data(
                self.i2c_addr, AuraI2CConfigConstants.AURA_BYTE_READ_ADDR)
            return bdata
        except OSError:
            # Error 6: No such device or address => XX
            return None

    def AuraRegisterWrite(self, register: int, value: int):
        # print('Writing address', hex(register), 'to 0x00')
        self.smbus.write_word_data(self.i2c_addr, 0x00,
                                   flip_endian_16(register))
        self.smbus.write_byte_data(
            self.i2c_addr, AuraI2CConfigConstants.AURA_BYTE_WRITE_ADDR, value)

    def AuraRegisterWriteBlock(self, register: int, data: list):
        self.smbus.write_word_data(self.i2c_addr, 0x00,
                                   flip_endian_16(register))
        self.smbus.write_i2c_block_data(self.i2c_addr, register, data)

    # rgb_list is a list of self.led_count (R,B,G) tuples
    def _set_rgb_data(self, BASE_ADDRESS, rgb_list):
        while(len(rgb_list) < self.led_count):
            rgb_list += rgb_list
        rgb_list = rgb_list[:self.led_count]
        # byte by byte:
        offset = 0
        for rgb in rgb_list:
            # for some reason, Aura RAM color regs store RBG instead of RGB.
            rbg = (rgb[0], rgb[2], rgb[1])
            for color in rbg:
                self.AuraRegisterWrite(BASE_ADDRESS + offset, color)
                offset += 1
        # block write mode (should be faster but isn't supported apparently?):
        # root@fowlmanor:~# i2cdetect -F 0
        # I2C Block Write                  no
        # flat_rgb_list = [item for sublist in rgb_list for item in sublist]
        # flat_rgb_list = [255 for i in range(self.led_count*3)]
        # interface.AuraRegisterWriteBlock(device, BASE_ADDRESS, flat_rgb_list)

    def set_rgb_data(self, control_mode, effect_mode, rgb_list, apply=True):
        # to maintain api compatibility with aurausbcontroller, set both data and modes here
        self.set_effect_mode(effect_mode)
        if control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT:
            self._set_rgb_data(self.AURA_REG_COLORS_EFFECT, rgb_list)
        elif control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            self._set_rgb_data(self.AURA_REG_COLORS_DIRECT, rgb_list)
        else:
            raise Exception('Unknown control mode: ' + str(control_mode))
        if apply:
            self.apply_changes()

    def get_rgb_data_by_mode(self, control_mode):
        print('Getting rgb data for control mode', control_mode, 'for', self)
        if control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT:
            return self._get_rgb_data(self.AURA_REG_COLORS_EFFECT)
        elif control_mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            return self._get_rgb_data(self.AURA_REG_COLORS_DIRECT)
        else:
            raise Exception('Unknown control_mode: ' + str(control_mode))

    def get_rgb_data(self):
        return self.get_rgb_data_by_mode(self.get_control_mode())

    def get_single_rgb_set(self, BASE_ADDRESS):
        # for some reason, Aura RAM color regs store RBG instead of RGB.
        R = self.AuraRegisterRead(BASE_ADDRESS)
        B = self.AuraRegisterRead(BASE_ADDRESS + 1)
        G = self.AuraRegisterRead(BASE_ADDRESS + 2)
        return (R, G, B)

    def _get_rgb_data(self, MODE_BASE_ADDRESS):
        rgb_sets = []
        for i in range(self.led_count):
            rgb_sets.append(self.get_single_rgb_set(MODE_BASE_ADDRESS + (i*3)))
        # print(rgb_sets)
        return rgb_sets

    def get_effect_mode(self):
        reg_mode = self.AuraRegisterRead(self.AURA_REG_MODE)
        for key, val in self.AURA_EFFECT_MODES.items():
            if val == reg_mode:
                print('RGB MODE: ' + key, val)
                return reg_mode
        print('Unknown RGB Mode. AURA_REG_MODE = ' + str(reg_mode))
        return reg_mode

    def get_control_mode(self):
        mode = self.AuraRegisterRead(self.AURA_REG_DIRECT)
        if mode is not None:
            if mode == self.AURA_CONTROL_MODE_EFFECT:
                print('Running in effect mode (0x8020=0)')
                return auracontrollerbase.AuraController.AURA_CONTROL_MODE_EFFECT
            elif mode == self.AURA_CONTROL_MODE_DIRECT:
                print('Running in direct mode (0x8020=1)')
                return auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT
            else:
                print('Unknown mode. Device ' + str(hex(self.i2c_addr)) +
                      ' - Mode (0x8020): ' + str(mode))
                return auracontrollerbase.AuraController.AURA_CONTROL_MODE_UNKNOWN
        else:
            print('Unknown mode. Device ' + str(hex(self.i2c_addr)) +
                  ' may be unavailable. 0x8020: ' + str(mode))
            return auracontrollerbase.AuraController.AURA_CONTROL_MODE_UNKNOWN

    def set_control_mode(self, mode, apply=True):
        # this is direct vs effect mode. ram: direct/effect = 0/1. usb: 3b/40. so gotta lookup
        if mode == auracontrollerbase.AuraController.AURA_CONTROL_MODE_DIRECT:
            self.AuraRegisterWrite(self.AURA_REG_DIRECT,
                                   self.AURA_CONTROL_MODE_DIRECT)
        else:
            self.AuraRegisterWrite(self.AURA_REG_DIRECT,
                                   self.AURA_CONTROL_MODE_EFFECT)
        if apply:
            self.apply_changes()

    def set_effect_mode(self, mode, apply=True):
        # this is one of the 14 modes in auracontrollerbase.py
        # don't need to change this since the values are the same for usb and ram
        self.AuraRegisterWrite(self.AURA_REG_MODE, mode)
        if apply:
            self.apply_changes()

    def apply_changes(self):
        self.AuraRegisterWrite(self.AURA_REG_APPLY, self.AURA_REG_APPLY_VAL)

    def dump_aura_device(self, start_range=AURA_ADDR_START_RANGE, end_range=AURA_ADDR_END_RANGE, outputfile=None, only_progress=False):
        o_file_handle = None
        if outputfile:
            o_file_handle = open(outputfile, 'w')
        device_header_line = 'SMBus number: {0} Device address: 0x{1:02x}'.format(self.bus_number, self.i2c_addr)
        device_line = 'Device name: {0}'.format(self.get_name_from_firmware())
        hex_header_line = '       {0}'.format(' '.join(["{0:2x}".format(i) for i in range(0x10)]))
        if not only_progress:
            print(device_header_line)
            print(device_line)
            print(hex_header_line)
        if o_file_handle:
            o_file_handle.write(device_header_line + '\n')
            o_file_handle.write(device_line + '\n')
            o_file_handle.write(hex_header_line + '\n')
        for i in range(start_range, end_range + 1, 0x10):
            byte_vals = []
            display_vals = []
            for j in range(0x10):
                b_val = self.AuraRegisterRead(i + j)
                if b_val is not None:
                    display_val = "{0:02x}".format(b_val)
                    display_vals.append(display_val)
                    byte_vals.append(b_val)
                else:
                    display_val = 'XX'
                    display_vals.append(display_val)
                    byte_vals.append(0x2e)
            ascii_chars = [self.get_chr(val) for val in byte_vals]
            print_line = "{0:04x}: {1} | {2}".format(i, ' '.join(display_vals), ''.join(ascii_chars))
            if o_file_handle:
                o_file_handle.write(print_line + '\n')
            if not only_progress:
                print(print_line)
            else:
                print('Dumping range {0:04x} of {1:04x} to {2:04x}'.format(i, start_range, end_range))
        if o_file_handle:
            o_file_handle.close()
            import os
            cwd = os.getcwd()
            outputfilepath = os.path.join(cwd, outputfile)
            print('Aura I2C dump for this device saved to {}'.format(outputfilepath))

    def get_chr(self, int_val):
        ch = chr(int_val)
        if ch.isprintable():
            return ch
        else:
            return '.'


def debug_dump():
    if len(sys.argv) > 2:
        bus_number = int(sys.argv[1])
        device = int(sys.argv[2], 16)
        friendly_name = 'AURA i2c RAM controller device - debugging'
        controller = AuraRAMController(bus_number, device, friendly_name)
        controller.dump_aura_device()
        return 0
    else:
        print('Library for interfacing with i2c aura devices. Running this as a file, provide the following for debug:')
        print('Usage: python3 auracontroller.py')
        return 1


def detect_and_init_devices():
    # real_detect - expose this better
    real_detect = True
    if real_detect:
        return real_detect_and_init_devices()
    else:
        return fake_detect_and_init_devices()

# linux only for now
def real_detect_and_init_devices():
    controllers = []
    for busnum in range(0, 5):
        try:
            for i2c_addr in range(0x00, 0x77 + 1):
                c = AuraRAMController(
                    busnum, i2c_addr)
                if c.is_valid_aura_device():
                    controllers.append(c)
        except OSError as err:
            print('smbus number', busnum,
                  'not found. can\'t add controllers on this smbus (eg. aura auxiliary controller).', err)
            break
        except ImportError as err:
            print('RAM i2c not implemented on Windows yet.')
            print('Skipping aura ram controller import/setup.', type(err), err)
            break
    print("I2C Aura Devices detected: ", controllers)
    return controllers


def fake_detect_and_init_devices():
    # for testing specific hardcoded values

    # (bus number, i2c address)
    AURA_CONTROLLER_RAM_ADDR = (0, 0x77)

    AURA_RAM_SLOT_LEFT1_ADDR = (0, 0x71)
    AURA_RAM_SLOT_LEFT2_ADDR = (0, 0x74)
    AURA_RAM_SLOT_LEFT3_ADDR = (0, 0x70)
    AURA_RAM_SLOT_LEFT4_ADDR = (0, 0x73)

    AURA_AUX_1_ADDR = (4, 0x4e)
    AURA_AUX_2_ADDR = (4, 0x6f)

    AURA_CONTROLLER_RAM = AuraRAMController(
        *AURA_CONTROLLER_RAM_ADDR, 'AuraControllerRam 0x77')
    AURA_RAM_SLOT_LEFT1 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT1_ADDR, 'AuraRamSlotLeft1')
    AURA_RAM_SLOT_LEFT2 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT2_ADDR, 'AuraRamSlotLeft2')
    AURA_RAM_SLOT_LEFT3 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT3_ADDR, 'AuraRamSlotLeft3')
    AURA_RAM_SLOT_LEFT4 = AuraRAMController(
        *AURA_RAM_SLOT_LEFT4_ADDR, 'AuraRamSlotLeft4')
    controllers = {}
    controllers['AuraRAM'] = [AURA_CONTROLLER_RAM]
    controllers['AuraRAMSlots'] = [AURA_RAM_SLOT_LEFT1,
                                   AURA_RAM_SLOT_LEFT2, AURA_RAM_SLOT_LEFT3, AURA_RAM_SLOT_LEFT4]

    try:
        AURA_AUX_1 = AuraRAMController(
            *AURA_AUX_1_ADDR, 'Aura Aux Unknown 1')
        AURA_AUX_2 = AuraRAMController(
            *AURA_AUX_2_ADDR, 'Aura Aux Unknown 2')
        controllers['AuraAUX'] = [AURA_AUX_1, AURA_AUX_2]
    except OSError as err:
        print('aux smbus not found. can\'t init aux aura controller.', err)

    return [controller for controller in ([controllers['AuraRAM']] + controllers['AuraRAMSlots']) if controller.is_valid_aura_device()]


if __name__ == "__main__":
    cs = real_detect_and_init_devices()
    print(cs)
    for c in cs:
        print(c.__json__())
    import sys
    if len(sys.argv) > 1:
        if sys.argv[1] == 'dump':
            for c in cs:
                c.dump_aura_device(start_range=0x0000, end_range=0xffff, outputfile='{0}-0x{1:02x}-{2}'.format(c.bus_number, c.i2c_addr, 'aurai2cdump.txt'), only_progress=False)
    exit(debug_dump())
