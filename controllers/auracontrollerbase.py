from abc import abstractmethod
from openpyaura.controllers.rgbcontrollerbase import RGBControllerBase
class AuraController(RGBControllerBase):
    friendly_name = 'GenericAuraController'

    device_type = 'AURA'

    # self defined
    AURA_CONTROL_MODE_EFFECT = 0x0
    AURA_CONTROL_MODE_DIRECT = 0x1
    AURA_CONTROL_MODE_UNKNOWN = -0x1

    AURA_EFFECT_MODE_UNKNOWN = -0x1

    AURA_EFFECT_MODES = {
        'OFF': 0,
        'STATIC': 1,
        'BREATHING': 2,
        'FLASHING': 3,
        'SPECTRUM CYCLE': 4,
        'RAINBOW': 5,
        'SPECTRUM CYCLE BREATHING': 6,
        'CHASE FADE': 7,
        'SPECTRUM CYCLE CHASE FADE': 8,
        'CHASE': 9,
        'SPECTRUM CYCLE CHASE': 10,
        'SPECTRUM CYCLE WAVE': 11,
        'CHASE RAINBOW PULSE': 12,
        'STARRY NIGHT': 13,
        'MUSIC': 14,
    }

    def __str__(self):
        return self.friendly_name

    def get_supported_effect_modes(self):
        return self.AURA_EFFECT_MODES

    @abstractmethod
    def dump_aura_device(self):
        pass

    @abstractmethod
    def get_effect_mode(self):
        pass

    @abstractmethod
    def get_control_mode(self):
        pass

    @abstractmethod
    def set_control_mode(self, mode):
        pass

    @abstractmethod
    def set_effect_mode(self, mode):
        pass
