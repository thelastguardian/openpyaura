from abc import ABC, abstractmethod

class RGBControllerBase(ABC):

    @abstractmethod
    def get_led_count(self):
        pass

    @abstractmethod
    def get_supported_effect_modes(self):
        pass

    @abstractmethod
    def get_rgb_data(self, mode):
        pass

    @abstractmethod
    def set_rgb_data(self, control_mode, effect_mode, rgb_list):
        """
        rgb_list is list of (R,B,G) tuples of hex bytes
        """
        pass

    @abstractmethod
    def apply_changes(self):
        pass
