#!/usr/bin/env python3

# from auracontrollerbase import AuraController
from openpyaura.daemon.auraremotehostconnection import AuraRemoteConnection
from openpyaura.daemon import daemoncommandmanagers as dcm

class AuraDaemonClient(object):
    name = ''
    aura_remote_connector = None
    command_manager = None

    def __init__(self, manager, host, port, command_manager='DAEMON'):
        self.manager = manager
        self.name = 'AuraDaemonClient-' + str(manager) + ' to target host: ' + str(host) + ':' + str(port)
        self.aura_remote_connection = AuraRemoteConnection(host, port)
        self.command_manager = command_manager
        print('Inited AuraDaemonClient to', host, port)

    def connect(self):
        return self.aura_remote_connection.connect()

    def shutdown(self):
        print('Triggering shutdown of ', self)
        self.aura_remote_connection.shutdown()

    def __str__(self):
        return self.name

    def is_connected(self):
        if not self.aura_remote_connection:
            return False
        else:
            return self.aura_remote_connection.alive()

    # gotta send a shutdown_remote() command before this connection is closed
    # when this object is being deleted (going out of scope after a get_list, etc), to avoid a 
    # socket closed error on the other side.
    def __del__(self):
        # super().__del__()
        if self.is_connected():
            self.shutdown_remote()

    def check_response(self, response):
        # print('------------')
        # print(self, response)
        if response['status'] == 'ok':
            return response['data']
        else:
            # print(response)
            raise Exception(response['error'])

    def send_to_remote(self, datadict, command_manager=None):
        if not command_manager:
            command_manager = self.command_manager
        datadict['sender'] = self.name
        datadict['command_manager'] = command_manager
        response = self.aura_remote_connection.send_to_remote(datadict)
        return self.check_response(response)

    def get_list_of_controllers(self):
        # self.trigger_init_of_remote_controllers()
        ret = self.send_to_remote({'command': 'get_list_of_controllers'})
        print(ret)
        return ret

    def trigger_init_of_remote_controllers(self):
        ret = self.send_to_remote({'command': 'init_controllers'}, command_manager='DAEMON')
        print(ret)
        return ret

    def get_status(self):
        ret = self.send_to_remote({'command': 'get_status'}, command_manager='DAEMON')
        print(ret)
        return ret

    def shutdown_remote(self):
        ret = self.send_to_remote({'command': 'shutdown'}, command_manager='DAEMON')
        print(ret)
        return ret

    def do_command(self, command, *args, **kwargs):
        ret = self.send_to_remote({'command': command, 'args': args, 'kwargs': kwargs})
        print(ret)
        return ret


# def main():
#     remotehost = AuraDaemonClient('test-client', '127.0.0.1', 8484)
#     remotehost.get_status()
#     remotehost.list_devices()
#     remotehost.shutdown_remote()
#     time.sleep(1)


# if __name__ == "__main__":
#     main()
