#!/usr/bin/env python3
import sys
import time
import threading

from openpyaura.controllers.auranetworkcontroller import AuraNetworkControllerSink, AuraNetworkController
from openpyaura.controllers.auracontrollerbase import AuraController
from openpyaura.utils import networkutils
from openpyaura.daemon.auradaemonclient import AuraDaemonClient
from openpyaura.daemon.auranetworkhostmanagerbase import AuraNetworkHostManager
from openpyaura.daemon.auracommandsink import AuraCommandSink
import openpyaura.daemon.daemoncommandmanagers as dcm
from openpyaura.daemon.daemoncommandmanagers import DaemonCommandManager, DaemonBasicModeCommandManager

remote_hosts_config = [
    {
        'host': '10.0.0.41',
        'port': 8484
    }
]

DAEMON_TYPE_MASTER = 'master'
DAEMON_TYPE_AGENT = 'agent'
# master daemons will init local devices and will also connect
# to agent daemons. to control these devices, connect straight to this daemon
# and issue commands, the devices themselves aren't networked like on agent daemons

# agent daemons will only init local devices as network sinks


class AuraDaemon(AuraCommandSink):
    daemon_type = DAEMON_TYPE_MASTER

    # network host to accept connections
    aura_network_host = None
    host = None
    port = None

    # local controllers
    ram_controllers = []
    usb_controllers = []

    # for master daemon:
    # remote hosts
    remote_host_clients = []
    # remote controllers across all remote hosts, if master daemon
    remote_controllers = []

    # for agent daemon:
    network_controller_sinks = []

    # status/control flags
    init_done = False
    stop_flag = False

    def __init__(self, daemon_type, host, port=8484):
        self.daemon_type = daemon_type
        self.command_managers = {
            dcm.TYPE_BASIC_MODE: DaemonBasicModeCommandManager(self, dcm.TYPE_BASIC_MODE, 'Daemon Basic Mode Command Manager'),
            dcm.TYPE_DAEMON: DaemonCommandManager(self, dcm.TYPE_DAEMON, 'Daemon Command Manager')
            }

        print('Initing Aura daemon.')
        self.host = host
        self.port = port

        # init local controllers

        # aura i2c
        from openpyaura.controllers import auraramcontroller
        self.ram_controllers = auraramcontroller.detect_and_init_devices()
        # aura usb
        from openpyaura.controllers import aurausbcontroller
        self.usb_controllers = aurausbcontroller.detect_and_init_devices()

        # rgbfusion gigabyte simple USB RGB device
        from openpyaura.controllers import gbusb
        self.usb_controllers += gbusb.detect_and_init_devices()

        print('RAM controllers:', self.ram_controllers)
        print('USB controllers:', self.usb_controllers)

        self.init_done = True
        print('Init done.')
        # print('Remote controllers so far:', self.remote_controllers)

    # for both master and agent daemons
    def _init_self_network_host(self):
        assert self.host is not None
        assert self.port is not None
        self.aura_network_host = AuraNetworkHostManager(
            self, self.host, self.port)

    # for master daemons
    def _init_remote_network_connection_clients(self):
        remote_hosts_to_try = [remote_host for remote_host in remote_hosts_config if remote_host['host'] != self.host]
        for remote_host in remote_hosts_to_try:
            print('Trying to contact', remote_host, 'for remote controllers')
            remote_host_client = AuraDaemonClient(
                self, remote_host['host'], remote_host['port'])
            self.remote_host_clients.append(remote_host_client)
            print('New remote host added: ', remote_host_client)
            # print('In auradaemon, trying to connect() created networkhostclient.')
            status = remote_host_client.connect()
            # print('status:', status)
            if status and not self.stop_flag:
                remote_controller_details = remote_host_client.get_list_of_controllers()
                for remote_controller_detail in remote_controller_details:
                    remote_controller = AuraNetworkController(
                        remote_controller_detail['controller_name'], remote_controller_detail['host'], remote_controller_detail['port'])
                    status = remote_controller.connect()
                    self.remote_controllers.append(remote_controller)

    # for agent daemons
    def _init_network_controller_sinks(self):
        free_ports = list(range(8485,8495))  # generate
        for controller in self.ram_controllers + self.usb_controllers:
            n_sink = AuraNetworkControllerSink(
                controller, self.host, free_ports.pop())
            self.network_controller_sinks.append(n_sink)

    def run(self, args):
        # init self server socket to accept command connections to this daemon
        self._init_self_network_host()

        if self.daemon_type == DAEMON_TYPE_MASTER and args.connect_remote:
            # according to the config, set up connections to manage remote aura network hosts (which host controllers on them)
            self.thread = threading.Thread(
                target=self._init_remote_network_connection_clients)
            self.thread.start()
        else:
            self._init_network_controller_sinks()

        # daemon startup stuff done, now run listener for daemon commands
        self.command_managers[dcm.TYPE_DAEMON].run(args)

    def __str__(self):
        return str(self.daemon_type) + ' AuraDaemon at ' + self.host + ':' + str(self.port)

    # def __repr__(self):
    #     return self.friendly_name + ' - ' + str(self.ram_controllers) + ', ' + str(self.usb_controllers) + ', ' + str(self.network_controller_sinks)

def launch_daemon(daemon_type, other_args):
    host = networkutils.get_usable_nonlocal_bind_ip()
    daemon = AuraDaemon(daemon_type=daemon_type, host=host)
    try:
        daemon.run(other_args)
    except Exception as err:
        print('Exception caught in daemon run: ', err)
        import traceback
        traceback.print_exc()
        daemon.command_managers[dcm.TYPE_DAEMON].shutdown()

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("daemon_type", help="master or agent", choices=['master', 'agent'], default='master')
    parser.add_argument("--connect_remote", help="connect to defined remote agent daemons")
    parser.add_argument("--run_effect", help="run test effect on daemon start")
    args = parser.parse_args()
    print('Running with arguments: ', args)
    launch_daemon(args.daemon_type, args)

if __name__ == "__main__":
    main()
