#!/usr/bin/env python3

class AuraCommandSink(object):
    command_managers = {}

    def execute_on(self, manager, jmsg):
        print(self, manager, jmsg)
        func = getattr(manager, jmsg['command'], None)
        if func:
            args = []
            kwargs = {}
            if 'args' in jmsg:
                args = jmsg['args']
            if 'kwargs' in jmsg:
                kwargs = jmsg['kwargs']
            ret = func(*args, **kwargs)
            return ret
        else:
            raise AttributeError(str(manager) + ' has no function ' + jmsg['command'])

    def act(self, jmsg):
        if 'command_manager' in jmsg:
            if jmsg['command_manager'] in self.command_managers:
                return self.execute_on(self.command_managers[jmsg['command_manager']], jmsg)
            else:
                raise AttributeError(str(self) + ' has no command_manager ' + jmsg['command_manager'])
        else:
            raise AttributeError('jmsg needs to contain the \'command_manager\' key to route this command: ' + jmsg)
