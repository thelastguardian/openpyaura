#!/usr/bin/env python3
import time
import threading

from openpyaura.controllers.auranetworkcontroller import AuraNetworkControllerSink, AuraNetworkController
from openpyaura.controllers.auracontrollerbase import AuraController
from openpyaura.utils import colors


# from auradaemon import 
DAEMON_TYPE_MASTER = 'MASTER'
DAEMON_TYPE_AGENT = 'AGENT'

# command manager types
TYPE_DAEMON = 'DAEMON'
TYPE_BASIC_MODE = 'BASIC'
TYPE_ADVANCED_MODE = 'ADVANCED'


class DaemonCommandManagerBase(object):
    def __init__(self, host_daemon, commandmanagertype: str, name: str):
        self.host_daemon = host_daemon
        self.commandmanagertype = commandmanagertype
        self.name = name

    def __str__(self):
        return str(self.host_daemon) + ' - ' + self.commandmanagertype + ' - ' + self.name

class DaemonCommandManager(DaemonCommandManagerBase):
    def launch_effect_threads(self):
        self.thread = threading.Thread(target=self.test_effect_func)
        self.thread.start()

    def run(self, args):
        if self.host_daemon.daemon_type == DAEMON_TYPE_MASTER and args.run_effect:
            self.launch_effect_threads()
        while not self.host_daemon.stop_flag:
            try:
                pass
                # print('event loop hmm')
                # print('Remote host clients so far:', self.remote_host_clients)
                # print('Remote controllers so far:', self.remote_controllers)
                time.sleep(1)
                import pdb
                pdb.set_trace()
            except KeyboardInterrupt as err:
                self.host_daemon.stop_flag = True
                self.shutdown()
        print('Exiting AuraDaemon in 2 seconds.')
        time.sleep(2)

    def shutdown(self):
        print('Triggering shutdown of ', self)

        # shutdown own network host
        print('Triggering shutdown of NetworkHostManager.')
        self.host_daemon.aura_network_host.shutdown()

        if self.host_daemon.daemon_type == DAEMON_TYPE_MASTER:
            print('Triggering shutdown of remote host clients:', self.host_daemon.remote_host_clients)
            [f.shutdown() for f in self.host_daemon.remote_host_clients]
            print('Triggering shutdown of connected network controllers:', self.host_daemon.remote_controllers)
            [f.shutdown() for f in self.host_daemon.remote_controllers]
        else:
            print('Triggering shutdown of hosted network controller sinks.')
            [f.shutdown() for f in self.host_daemon.network_controller_sinks]

    def test_effect_func(self):
        time.sleep(1)
        print('starting test effect')
        try:
            while True and not self.host_daemon.stop_flag:
                for color in [colors.red, colors.green, colors.blue]:
                    for controller in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers:
                        print('Applying to', controller)
                        controller.set_rgb_data(AuraController.AURA_CONTROL_MODE_EFFECT,
                                                AuraController.AURA_EFFECT_MODES['STATIC'], [color])

                    time.sleep(1)
        except KeyboardInterrupt:
            print('done test effect')

    def get_status(self):
        return self.host_daemon.init_done

    def get_list_of_controllers(self):
        # controller objects are not json serializable, see if it can be done without decoder class hacks.
        if self.host_daemon.daemon_type == DAEMON_TYPE_MASTER:
            # return all controllable devices
            return [d.__json__() for d in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers]
        else:
            # return hosted network controller sinks
            return [d.__json__() for d in self.host_daemon.network_controller_sinks]


class DaemonBasicModeCommandManager(DaemonCommandManagerBase):
    ALL_CONTROLLERS_DEVICE = {'controller_name': 'All devices', 'id': 'ALLDEVICES', 'type': 'DUMMY'}
    NO_SELECTED_DEVICE = {'controller_name': 'Choose a device', 'id': 'UNSELECTED', 'type': 'DUMMY'}

    # for both master and agent daemons
    def get_list_of_controllers(self):
        # when within basic mode, we get requests from cli/gui clients, to which
        # we should return the true controllers, whether the daemon is master or agent, not the network sinks
        return [self.ALL_CONTROLLERS_DEVICE] + [d.__json__() for d in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers]
        # controller objects are not json serializable, see if it can be done without decoder class hacks.

    def get_controller_by_id(self, controller_id):
        for controller in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers:
            if controller.device_id == controller_id:
                return controller
        print("Couldn't find controller in daemon!", controller_id)
        return None

    def get_rgb_data(self, device):
        print('DaemonCommandManager-get_rgb_data', device)
        if device is None or device['id'] == self.ALL_CONTROLLERS_DEVICE['id'] or device['id'] == self.NO_SELECTED_DEVICE['id']:
            rgb_data = [colors.black] * 5
        else:
            controller = self.get_controller_by_id(device['id'])
            rgb_data = controller.get_rgb_data()
        print('returning', rgb_data)
        return rgb_data

    def set_rgb_data(self, device, control_mode, effect_mode, rgb_data):
        print('DaemonCommandManager-set_rgb_data', device, rgb_data)
        if device is None or device['id'] == self.ALL_CONTROLLERS_DEVICE['id'] or device['id'] == self.NO_SELECTED_DEVICE['id']:
            print('Setting rgb data to all controllers.')
            for controller in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers:
                controller.set_rgb_data(control_mode, effect_mode, rgb_data)
            print('Done.')
        else:
            controller = self.get_controller_by_id(device['id'])
            res = controller.set_rgb_data(control_mode, effect_mode, rgb_data)
        print('done', rgb_data)
        return True

    def set_effect_mode(self, device, effect_mode):
        print('DaemonCommandManager-set_effect_mode', device, effect_mode)
        if device is None or device['id'] == self.ALL_CONTROLLERS_DEVICE['id'] or device['id'] == self.NO_SELECTED_DEVICE['id']:
            print('Setting effect_mode to all controllers.')
            for controller in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers:
                res = controller.set_effect_mode(effect_mode)
            print('Done.')
        else:
            controller = self.get_controller_by_id(device['id'])
            res = controller.set_effect_mode(effect_mode)
        print('done', effect_mode)
        return True

    def get_effect_mode(self, device):
        print('DaemonCommandManager-get_effect_mode', device)
        if device is None or device['id'] == self.ALL_CONTROLLERS_DEVICE['id'] or device['id'] == self.NO_SELECTED_DEVICE['id']:
            mode = AuraController.AURA_EFFECT_MODES['OFF']
            # print('Setting effect_mode to all controllers.')
            # for controller in self.host_daemon.ram_controllers + self.host_daemon.usb_controllers + self.host_daemon.remote_controllers:
            #     res = controller.get_effect_mode()
            # print('Done.')
        else:
            controller = self.get_controller_by_id(device['id'])
            mode = controller.get_effect_mode()
        print('returning', mode)
        return mode
