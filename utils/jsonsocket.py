import struct
import json

def read_blob(sock, size):
    buf = b''
    # return sock.recv(size)
    while len(buf) != size:
        # print('left to receive: ' + str(size-len(buf)))
        ret = sock.recv(size - len(buf))
        if not ret:
            raise Exception("Socket closed")
        buf += ret
    # print('buf=' + str(buf))
    return buf


def read_long(sock):
    size = struct.calcsize("!L")
    # print('long_size = ' + str(size))
    data = read_blob(sock, size)
    return struct.unpack("!L", data)[0]


def read_json(sock):
    length = read_long(sock)
    # print('Attempting to read', length, 'bytes of JSON.')
    data = read_blob(sock, length)
    # print('Received:'+ str(data))
    decoded = data.decode()
    # print('Decoded:'+ str(decoded))
    # print(type(decoded))
    try:
        return json.loads(decoded)
    except (TypeError, ValueError):
        raise Exception('Data received was not in JSON format')

def write_long(sock, data_size):
    size = struct.calcsize("!L")
    # print('long_size = ' + str(size))
    data = struct.pack("!L", data_size)
    # print(data)
    sock.sendall(data)

def write_json(sock, data):
    print('Sending: ', data)
    #
    # except (TypeError, ValueError):
    #   raise Exception('You can only send JSON-serializable data')
    size = len(json.dumps(data))
    write_long(sock, size)
    b_sent = sock.sendall(json.dumps(data).encode())
    return (len(json.dumps(data)), b_sent)
