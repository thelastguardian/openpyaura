#!/usr/bin/env python3
import socket

def get_usable_nonlocal_bind_ip():
    return ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])


# https://stackoverflow.com/questions/22878625/receiving-broadcast-packets-in-python
def advertise():
    import socket
    s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.sendto('this is testing'.encode(),('255.255.255.255',12346))
    print('sent')

def detect():
    import socket
    s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('255.255.255.255',12345))
    m=s.recvfrom(1024)
    print(m[0])

def test():
    advertise()

    import socket
    s=socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(('',12346))
    print('recvfrom-ing')
    m=s.recvfrom(1)
    print(m[0])

if __name__ == "__main__":
    test()