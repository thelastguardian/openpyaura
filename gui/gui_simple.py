#!/usr/bin/env python3

import tkinter as tk
from tkinter import ttk
import queue

from openpyaura.controllers.auracontrollerbase import AuraController as acbac
from openpyaura.utils import colors

class OpenManagerGUI(object):
    devices = None
    def __init__(self, manager):
        self.event_queue = queue.Queue(maxsize=0)
        self.manager = manager
        self.window = tk.Tk()
        self.window.title('AuraOpenManager')
        self.window.geometry('500x500')

        self.top_frame = tk.Frame(self.window)
        self.top_frame.pack(anchor=tk.N)

        self.frame_setbuttons = tk.Frame(self.top_frame)
        self.frame_setbuttons.pack(anchor=tk.W, side=tk.LEFT)
        self.btn_setall = tk.Button(self.frame_setbuttons, text="Set All", command=lambda: self.manager.set_all(self.get_selected_device(), self.get_rgb_grid(), self.get_mode_data()))
        self.btn_setall.pack(side=tk.LEFT)
        # self.btn_setcolors = tk.Button(self.frame_setbuttons, text="Set Colors", command=self.manager.set_colors)
        # self.btn_setcolors.pack(side=tk.RIGHT)

        self.frame_devices_and_label = tk.Frame(self.top_frame)
        self.frame_devices_and_label.pack(anchor=tk.E, side=tk.RIGHT)
        self.label_device = tk.Label(self.frame_devices_and_label, text="Device Selection")
        choices = []
        # var = tk.StringVar(self.frame_devices_and_label)
        # # initial value
        # var.set('red')
        self.combobox_devices = ttk.Combobox(self.frame_devices_and_label, values=choices)
        # can't set current = 0 when no options
        # self.combobox_devices.current(0)
        self.label_device_type = tk.Label(self.frame_devices_and_label, text="? Device Type")
        self.label_device.pack()
        self.combobox_devices.pack(side=tk.LEFT)
        self.label_device_type.pack(side=tk.RIGHT)

        # self.var_effectmodeselect = tk.IntVar()
        self.frame_rgbinputsets = tk.LabelFrame(self.window, text="RGB")
        self.frame_rgbinputsets.pack(anchor=tk.W)
        self.rgb_inputsets = []
        colors = ['Red', 'Green', 'Blue']
        self.color_entries = []
        for j in range(1,4):
            lbl = tk.Label(self.frame_rgbinputsets, text=colors[j-1])
            lbl.grid(row=0, column=j)
        for i in range(1,6):
            for j in range(1,4):
                entry = tk.Entry(self.frame_rgbinputsets)
                entry.grid(row=i, column=j)
                self.color_entries.append(entry)

        self.var_effectmodeselect = tk.IntVar()
        self.frame_effectmodes = tk.LabelFrame(self.window, text="Internal Effect Modes")
        self.frame_effectmodes.pack(side=tk.RIGHT)
        self.effectmode_radiobtns = {}
        for k, v in acbac.AURA_EFFECT_MODES.items():
            radiobtn = tk.Radiobutton(self.frame_effectmodes, text=k, variable=self.var_effectmodeselect, value=v)
            radiobtn.pack(anchor=tk.W)
            # self.effectmode_radiobtns[v] = radiobtn

        self.frame_daemoncontrols = tk.LabelFrame(self.window, text="Daemon Controls")
        self.frame_daemoncontrols.pack()
        self.label_daemonstatus = tk.Label(self.frame_daemoncontrols, text="Click Connect to connect to daemon")
        self.label_daemonstatus.pack()
        self.label_outputtext = tk.Label(self.frame_daemoncontrols, text="Output")
        self.label_outputtext.pack()
        self.btn_daemonreconnect = tk.Button(self.frame_daemoncontrols, text="Connect to daemon", command=self.manager.connect_daemon_client)
        self.btn_daemonreconnect.pack(side=tk.LEFT)
        self.btn_daemonrefresh = tk.Button(self.frame_daemoncontrols, text="Refresh", command=lambda: self.manager.update_gui(self.get_selected_device()))
        self.btn_daemonrefresh.pack(side=tk.RIGHT)

        self.btn_test = tk.Button(self.frame_daemoncontrols, text="test", command=self.manager.test_callback)
        self.btn_test.pack(side=tk.BOTTOM)
        # self.label_test.pack()
        # self.label_test = tk.Label(self.window, text="test")

    def test_func(self, *args, **kwargs):
        print('args', args)
        print('kwargs', kwargs)
        self.btn_test.configure(text=str(args))

    def get_selected_device(self):
        device_display_val = self.combobox_devices.get()
        if device_display_val.strip() != '':
            device_name, device_id = device_display_val.split('|')
            device_data = self.get_device_data_by_id(device_id)
        else:
            device_data = None
        return device_data

    def get_device_data_by_id(self, device_id):
        if self.devices:
            for device in self.devices:
                if device['id'] == device_id:
                    return device
        print("Couldn't find device in local list!", device_id, self.devices)
        return None

    def set_lbl_outputtext(self, text):
        self.label_outputtext.configure(text=text)

    def set_window_title(self, title):
        self.window.title(title)

    def set_devices_list(self, devices_list):
        # devices_list is a list of tuples (name, type)
        # name: duh, type: 'Aura', 'Generic', etc
        # TODO: set the device type label next to the dropdown
        # whenever the selection changes or list is updated
        device_names = [device['controller_name'] + '|' + device['id'] for device in devices_list]
        self.combobox_devices.configure(values=device_names)
        self.devices = devices_list
        # self.combobox_devices.current(0)

    def set_rgb_grid(self, rgb_list):
        # rgb list is a list of tuples
        count = 0
        for rgb in rgb_list:
            for color in rgb:
                self.color_entries[count].delete(0, tk.END)
                self.color_entries[count].insert(0, color)
                count += 1

    def set_mode_data(self, mode_data):
        # mode_data is int
        self.var_effectmodeselect.set(mode_data)

    def get_rgb_grid(self):
        rgb_data = []
        len_tuples = len(self.color_entries) // 3
        for i in range(len_tuples):
            rgb_data.append((int(self.color_entries[i*3].get()), int(self.color_entries[(i*3) + 1].get()), int(self.color_entries[(i*3) + 2].get())))
        print('saved rgb grid data:', rgb_data)
        return rgb_data

    def get_mode_data(self):
        return self.var_effectmodeselect.get()

    def on_after_elapsed(self):
        while True:
            try:
                event = self.event_queue.get(timeout=0.1)
            except:
                break
            print(event)
            func = event['func']
            # print(func)
            func(*event.get('args', []), **event.get('kwargs', {}))
        self.window.after(100, self.on_after_elapsed)


    def run(self):
        self.window.after(100, self.on_after_elapsed)
        self.window.mainloop()
