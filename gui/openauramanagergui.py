#!/usr/bin/env python3
import threading
import traceback

from openpyaura.controllers.auracontrollerbase import AuraController as acbac
from openpyaura.daemon.auradaemonclient import AuraDaemonClient
from openpyaura.daemon.auracommandsink import AuraCommandSink
from openpyaura.gui.gui_simple import OpenManagerGUI
from openpyaura.utils import colors

class AuraOpenManagerGUI(AuraCommandSink):
    host = '10.0.0.40'
    port = 8484

    def __init__(self):
        print('Initing AuraOpenManager')
        self.launch_window = OpenManagerGUI(self)
        self.init_daemon_client()
        # self.connect_daemon_client()

        #
        self.count = 0

    def init_daemon_client(self):
        self.daemon_client = AuraDaemonClient(self, self.host, self.port, command_manager='BASIC')

    def connect_daemon_client(self):
        try:
            if self.daemon_client.is_connected():
                print('Reconnecting.')
                self.daemon_client.shutdown()
                self.init_daemon_client()
                self.daemon_client.connect()
            else:
                print('Connecting.')
                self.daemon_client.connect()
            data = { 'func': self.launch_window.set_lbl_outputtext, 'args': ['Connected']}
            self.launch_window.event_queue.put(data)
        except ConnectionRefusedError as err:
            # print(err)
            traceback.print_exc()
            data = { 'func': self.launch_window.set_lbl_outputtext, 'args': [str(err)]}
            self.launch_window.event_queue.put(data)

    def check_warn_unconnected_daemon_client(self):
        if not self.daemon_client.is_connected():
            data = { 'func': self.launch_window.set_lbl_outputtext, 'args': ['Daemon is not connected. Please click connect first.']}
            self.launch_window.event_queue.put(data)

    def run(self):
        self.thread = threading.Thread(target=self.background)
        self.thread.start()
        self.gui()

    def gui(self):
        self.launch_window.run()

    def background(self):
        print('Background thread')

    # def refresh_data(self):
    #     pass

    def set_all(self, device, rgb_list, effect_mode):
        self.check_warn_unconnected_daemon_client()
        print('set_all')
        resp = self.daemon_client.do_command('set_rgb_data', device, acbac.AURA_CONTROL_MODE_EFFECT, effect_mode, rgb_list)
        # is setting the effect mode separately again really needed: ?
        resp = self.daemon_client.do_command('set_effect_mode', device, effect_mode)
        # what response should this return?
        return resp

    # called when a device is selected in the gui, or the refresh button is clicked.
    def update_gui(self, device):
        self.check_warn_unconnected_daemon_client()
        print('update_gui', device)
        self.update_mode_data(device)
        self.update_rgb_grid(device)
        self.update_devices_list()

    def update_devices_list(self):
        devices_list = self.daemon_client.get_list_of_controllers()
        data = { 'func': self.launch_window.set_devices_list, 'args': [devices_list]}
        self.launch_window.event_queue.put(data)

    def update_rgb_grid(self, device):
        resp = self.daemon_client.do_command('get_rgb_data', device)
        data = { 'func': self.launch_window.set_rgb_grid, 'args': [resp]}
        self.launch_window.event_queue.put(data)

    def update_mode_data(self, device):
        resp = self.daemon_client.do_command('get_effect_mode', device)
        data = { 'func': self.launch_window.set_mode_data, 'args': [resp]}
        self.launch_window.event_queue.put(data)

    def test_callback_text(self):
        self.count += 1
        print('test' + str(self.count))
        self.launch_window.event_queue.put('test' + str(self.count))

    def test_callback(self):
        data = { 'func': self.launch_window.test_func, 'args': ['bla', '2'], 'kwargs': {'a': 'b', 'c': 'd'}}
        self.launch_window.event_queue.put(data)

def main():
    aura_open_manager_gui = AuraOpenManagerGUI()
    aura_open_manager_gui.run()

if __name__ == "__main__":
    main()